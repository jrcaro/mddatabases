SET FOREIGN_KEY_CHECKS = 0;
load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/correquisitos.csv'
INTO TABLE es_correquisito
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/investigadores.csv'
INTO TABLE investigadores
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/matricular.csv'
INTO TABLE matricular
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/prerrequisito.csv'
INTO TABLE es_prerrequisito
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/departamentos.csv'
INTO TABLE departamentos
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/materias.csv'
INTO TABLE materias
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/municipio.csv'
INTO TABLE municipios
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/profesores.csv'
INTO TABLE profesores
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/alumnos.csv'
INTO TABLE alumnos
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/asignaturas.csv'
INTO TABLE asignatura
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/impartir.csv'
INTO TABLE impartir
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

load data infile 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/provincia.csv'
INTO TABLE provincia
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;
