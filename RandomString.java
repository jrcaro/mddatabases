package uma.org.mbd;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class RandomString {

    // function to generate a random string of length n
    static String getAlphaNumericString()
    {

        // chose a Character random from this String
        String letterString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String numericString = "0123456789";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(7);

        for (int i = 0; i < 7; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            if(i < 4) {
                int indexN
                        = (int) (numericString.length()
                        * Math.random());

                // add Character one by one in end of sb
                sb.append(numericString
                        .charAt(indexN));
            } else {
                int indexL
                        = (int) (letterString.length()
                        * Math.random());

                // add Character one by one in end of sb
                sb.append(letterString
                        .charAt(indexL));
            }
        }

        return sb.toString();
    }

    public static void main(String[] args) throws IOException
    {
        try(
                Reader reader = Files.newBufferedReader(Paths.get("C:\\Users\\master\\IdeaProjects\\software\\src\\main\\java\\uma\\org\\mbd\\vehiculos.csv"));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);){
            // Get and display the alphanumeric string
            System.out.println(RandomString
                    .getAlphaNumericString());
        }



    }
}