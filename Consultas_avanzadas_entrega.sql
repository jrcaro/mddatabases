#Entrega avanzada
#1
select im.asignatura
from impartir im
where im.profesor in 
(select p.id
from profesores p
where UPPER(p.nombre) = 'Manuel' and
		UPPER(p.apellido1) = 'Enciso');
        
#2
select m.alumno CodigoAlumno
from matricular m
where m.asignatura in
(select im.asignatura
from impartir im
where m.curso = im.curso and
		m.grupo = im.grupo and
        im.profesor in
(select p.id
from profesores p
where p.id = 'C-34-TU-00'));

#3
select d.nombre Departamento, sum(a.creditos) 'Numero creditos'
from asignatura a 
		join departamentos d on (a.departamento = d.codigo)
group by d.nombre;

#4
select distinct p.despacho 'Despacho', sum(im.carga_creditos) 'Numero de creditos'
from profesores p join impartir im on (p.id = im.profesor)
where p.despacho is not null
group by p.despacho;

#5
select distinct concat(al.nombre, ' ', al.apellido1, ' ' , ifnull(al.apellido2, ' ')) Alumno, a.nombre Asignatura
from asignatura a, alumnos al, matricular m
where (a.codigo = m.asignatura and
		m.alumno = al.dni and
        (al.dni, a.creditos) in
(select m.alumno, max(a.creditos)
from asignatura a join matricular m on (a.codigo = m.asignatura)
group by m.alumno));

#6
select distinct d.nombre Departamento, a.nombre Asignatura
from departamentos d, asignatura a
where d.codigo = a.departamento and
		(d.codigo, a.creditos) in
(select d.codigo, min(a.creditos)
from departamentos d, asignatura a
where d.codigo = a.departamento
group by d.codigo);

#7
select d.nombre
from departamentos d join asignatura a on (d.codigo = a.departamento)
group by d.nombre
having count(d.nombre) =
(select max(temp.contador)
from
(select count(d.nombre) 'contador'
from departamentos d join asignatura a on (d.codigo = a.departamento)
group by d.nombre) as temp);

#8
select p.nombre Nombre, p.apellido1 Apellido1, ifnull(p.apellido2, ' ') Apellido2
from profesores p join impartir im on (p.id = im.profesor)
group by p.nombre, p.apellido1
having sum(im.carga_creditos) > (
select avg(temp.suma)
from
(select sum(carga_creditos) 'suma'
from impartir
group by profesor) as temp);

#9
select distinct d.nombre
from departamentos d join asignatura a on (d.codigo = a.departamento)
where a.departamento not in
(select departamento
from asignatura 
where creditos > 6);

#10****
select concat(p1.nombre, ' ', p1.apellido1, ' ', ifnull(p1.apellido2, ' ')) Profesor1,
		concat(p2.nombre, ' ', p2.apellido1, ' ', ifnull(p2.apellido2, ' ')) Profesor2
from profesores p1, profesores p2 
where p1.id > p2.id and 
select distinct a.dni
from alumnos a join matricular m on (a.dni = m.alumno)
				join impartir im on (m.asignatura = im.asignatura)
				join profesores p on (im.profesor = p.id);
