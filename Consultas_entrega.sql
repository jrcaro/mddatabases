#Consultas
#1
select p.id, p.nombre, p.APELLIDO1, p.APELLIDO2, im.ASIGNATURA, asi.NOMBRE
from profesores p, impartir im, asignatura asi
where p.id = im.profesor and
		im.asignatura = asi.codigo;
        
#2
select p.nombre, ifnull(p.DIRECTOR_TESIS,'No tiene') Director
from profesores p;

#3
select p1.nombre nombreP1, p1.apellido1 apellido1P1, p1.apellido2 apellido2P1, TIMESTAMPDIFF(year, p1.ANTIGUEDAD, sysdate()) antiguiedadP1,
		p2.nombre nombreP2, p2.apellido1 apellido1P2, p2.apellido2 apellido2P2, TIMESTAMPDIFF(year, p2.ANTIGUEDAD, sysdate()) antiguiedadP2
from profesores p1, profesores p2
where p1.id < p2.id and #condicion reflexiva
		abs(TIMESTAMPDIFF(year, p1.ANTIGUEDAD, p2.ANTIGUEDAD)) < 2 and
        p1.departamento = p2.departamento;
        
#4
select a1.nombre Asignatura1, a2.nombre Asignatura1, a3.nombre Asignatura1, a1.COD_MATERIA Materia
from asignatura a1, asignatura a2, asignatura a3
where a1.codigo < a2.codigo and
		a1.codigo < a3.codigo and
        a2.codigo < a3.codigo and
        a1.COD_MATERIA = a2.COD_MATERIA and
        a2.COD_MATERIA = a3.COD_MATERIA and
        a3.COD_MATERIA = a1.COD_MATERIA;
        
#5
select a.nombre, a.apellido1, a.apellido2
from alumnos a
		join matricular m on (a.dni = m.alumno)
        join impartir im on (m.asignatura = im.asignatura and
								m.curso = im.curso and
                                m.grupo = im.grupo)
		join profesores p on (p.id = im.profesor and 
								UPPER(p.nombre) = 'Enrique' and
                                UPPER(p.apellido1) = 'Soler')
order by a.apellido1, a.apellido2, a.nombre;

#6
select a.nombre Asignatura, m.nombre Materia, 
		concat(p.nombre, ' ',p.apellido1, ' ', ifnull(p.apellido2, ' ')) Profesor, 
        im.carga_creditos  
from asignatura a
		join materias m on (a.cod_materia = m.codigo)
        join impartir im on (im.asignatura = a.codigo and im.carga_creditos is not null)
        join profesores p on (im.profesor = p.id)
order by a.cod_materia, a.nombre desc;

#7
select a.nombre Asignatura, d.nombre Departamento, a.creditos Creditos, 
		truncate((a.practicos/a.creditos)*100, 2) '%Practicos'
from asignatura a
		join departamentos d on (a.departamento = d.codigo and 
									a.creditos is not null and
                                    a.teoricos is not null and
									a.practicos is not null)
order by truncate((a.practicos/a.creditos)*100, 2) desc;
    
#8
(select a.apellido1 Apellidos
from alumnos a
where UPPER(a.apellido1) like '%LL%')
union
(select a.apellido2 Apellidos
from alumnos a
where UPPER(a.apellido2) like '%LL%')
union
(select p.apellido1 Apellidos
from profesores p
where UPPER(p.apellido1) like '%LL%')
union
(select p.apellido1 Apellidos
from profesores p
where UPPER(p.apellido1) like '%LL%');

#9
select concat('El Director de ', p1.nombre, ' ' ,p1.apellido1, ' ' , ifnull(p1.apellido2, ' '), 'es ',
				p2.nombre, ' ' ,p2.apellido1, ' ' , ifnull(p2.apellido2, ' ')) TESIS, ifnull(i.tramos, 0) TRAMOS
from profesores p1 join profesores p2 on (p1.director_tesis = p2.id)
		left outer join investigadores i on (p2.id = i.id_profesor);
        
#10
select a1.nombre, a1.apellido1, a1.apellido2, a2.nombre, a2.apellido1, a2.apellido2
from alumnos a1 left outer join alumnos a2 on (a1.dni < a2.dni and a1.FECHA_PRIM_MATRICULA = a2.FECHA_PRIM_MATRICULA)
order by a1.apellido1, a1.apellido2, a1.nombre;