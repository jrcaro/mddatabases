#Consultas avanzadas
#1
select a1.nombre, a1.codigo
from asignatura a1
where exists (select *
			from asignatura a2
			where ifnull(a1.curso, 0) = ifnull(a2.curso, 0) and
					ifnull(a2.creditos, 0) > ifnull(a1.creditos, 0));
/*where a1.creditos > all(select a2.creditos
					from asignatura a2
                    where ifnull(a1.curso, 0) = ifnull(a2.curso, 0)) cuidado con los null*/

#2
select nombre, creditos
from asignatura
where creditos > all (select creditos from asignatura where curso = 2); #no hay relacion entre las dos consultas, no es necesario ponerle alias

#13
select nombre
from asignatura
where codigo not in (select asignatura from matricular where calificacion != 'SP');

#14 tendria que salir 1
select p.nombre, p.apellido1, p.apellido2
from profesores p join impartir im on (p.id = im.profesor) 
join asignatura a on (im.asignatura = a.codigo) # profesores que imparten en cada asignatura
where a.caracter = 'OP' and 
(im.asignatura, im.grupo) not in (select asignatura, grupo from matricular);

#3
select d.nombre NombreDept, count(*) NumeroProf
from profesores p, departamentos d
where p.departamento = d.codigo
group by d.codigo, d.nombre;

#4
select a.curso, count(distinct m.alumno) Cantidad_Alumnos
from matricular m join asignatura a on (m.asignatura = a.codigo)
where a.curso is not null
group by a.curso;

#7 agrupacion 2 igual
select d.nombre, p.nombre, p.apellido1, p.apellido2
from departamentos d join profesores p on (p.DEPARTAMENTO = d.codigo)
where (p.departamento, p.FECHA_NACIMIENTO) in

(select departamento, min(FECHA_NACIMIENTO)
from profesores
group by departamento); #clave primaria

#10
select p.nombre, p.apellido1, sum(im.carga_creditos)
from profesores p join impartir im on (p.id = im.profesor)
group by p.id, p.nombre, p.apellido1
having sum(im.carga_creditos) = (
select max(temp.suma)
from (select sum(carga_creditos) 'suma' #no se puede encadenar fucniones de agregacion max(sum())
from impartir
group by profesor) as temp);