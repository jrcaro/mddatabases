#Consulta 1
select p.nombre, p.apellido1
from profesores p, departamentos d
where  p.departamento=d.codigo
		and d.nombre='Lenguajes y Ciencias de la Computacion'; #UPPER(d.nombre) LIKE '%LENGUAJES%'
        
#Consulta 2
select asi.nombre, asi.codigo, ifnull(asi.practicos,'No tiene') as creditos
from alumnos al, matricular m, asignatura asi
where al.dni=m.alumno and 
		m.asignatura=asi.codigo and
        UPPER(al.nombre)='Nicolas'and
        UPPER(al.apellido1)='Bersabe' and
        UPPER(al.apellido2)='Alba';
        
#Consulta 3
#select al.NOMBRE, m.CALIFICACION
#from alumnos al, asignitura asi, matricular m, 
#where al.dni=m.alumno and
#		asi.codigo=m.asignatura and
#        UPPER(asi.nombre)='Bases de Datos' and
#        m.calificacion !='NP';

#Consulta 4
select a1.nombre, 
		truncate(timestampdiff(month,a1.fecha_nacimiento,sysdate())/12,0) edadA1,
        a2.nombre,
		truncate(timestampdiff(month,a2.fecha_nacimiento,sysdate())/12,0) edadA2
from alumnos a1, alumnos a2
where UPPER(a1.apellido1)=UPPER(a2.apellido1) and 
		a1.dni < a2.dni;
        
#Consulta 6
select concat(a1.nombre, ' ', a1.apellido1, ' ', ifnull(a1.apellido2, ' ')) Ella, 
		concat(a2.nombre, ' ', a2.apellido1, ' ', ifnull(a2.apellido2, ' ')) El
from alumnos a1, alumnos a2, matricular m1, matricular m2 #self join, condicion reflexiva
where m1.alumno = a1.dni and
        m2.alumno = a2.dni and
        m1.asignatura = 112 and
		m2.asignatura = 112 and
		a1.genero = 'FEM' and #condicion reflexiva. evita duplicidad
        a2.genero = 'MASC' and
        week(a1.FECHA_PRIM_MATRICULA) = week(a1.FECHA_PRIM_MATRICULA) and
        case 
			when m1.calificacion = 'SP' then 1
            when m1.calificacion = 'AP' then 2
            when m1.calificacion = 'MH' then 5
            when m1.calificacion = 'NT' then 4
            when m1.calificacion = 'SB' then 3
            else 0 end >
		case 
			when m2.calificacion = 'AP' then 2
            when m2.calificacion = 'SP' then 1
            when m2.calificacion = 'MH' then 5
            when m2.calificacion = 'NT' then 4
            when m2.calificacion = 'SB' then 3
            else 0 end;        
        
#Consulta 10
select asi.nombre, im.profesor #p.nombre
from asignatura asi left outer join impartir im on (asi.codigo=im.asignatura) 
#from asignatura asi left outer join (impartir im join profesores p on (im.profesor=p.id)) on (asi.codigo=im.asignatura)
where ifnull(asi.creditos,0) != ifnull(asi.teoricos,0) + ifnull(asi.practicos,0);

#Consulta 11
select concat(p1.nombre, ' ', p1.apellido1) Profesor,
		ifnull(concat(p2.nombre, ' ', p2.apellido1), 'No tiene') Director
from profesores p1 left outer join profesores p2 on (p1.director_tesis = p2.id) #si fuera reflexiva no saldria un profesor que no tenga tesis
																				#clave foranea del primero (left) con la clave pimeria del segundo
order by 1; #ordena respecto al primer atributo del select. 

		